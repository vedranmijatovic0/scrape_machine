# Script that searches for meme images and scrolls every 10 seconds
require 'watir'

browser = Watir::Browser.new(:chrome)
browser.goto('duckduckgo.com')
browser.text_field(name: 'q').set 'memes'
browser.send_keys :enter
browser.link(text: 'Images').click
10.times do |i|
  sleep 10
  browser.driver.execute_script("window.scrollBy(0,1000)")
end

print "press any key to EXIT"
STDIN.getc