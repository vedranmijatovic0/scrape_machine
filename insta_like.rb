# Script that loggs in to Instagram and likes first 5 posts if they are images
require 'watir'

browser = Watir::Browser.new(:chrome)
browser.goto('instagram.com')
browser.button(text: 'Accept').click

# Log in with instagram login form
# browser.text_field(name: 'username').set 'your email here'
# browser.text_field(name: 'password').set 'your password here'
# browser.button(text: 'Log In').click

# OR

# Log in with facebook account
browser.button(index: 1).click
browser.button(text: 'Accept All').click
browser.text_field(name: 'email').set 'your email here'
browser.text_field(name: 'pass').set 'your password here'
browser.button(name: 'login').click
browser.button(text: 'Not Now').click

5.times do |i|
  sleep 2
  browser.article(role: 'presentation', index: i).double_click
end

print "press any key to EXIT"
STDIN.getc