# Mini Scrape Machine
This is a mini demo for a lecture about web crawlers. It uses Ruby programming language and Watir gem.

The code is not optimized, and its repetitive parts could be wrapped up in methods.

## Getting Started
Install [Ruby](https://www.ruby-lang.org/en/)

Install Watir ruby gem with: ```gem install watir```

For more help use [Watir documentation](http://watir.com/guides/)